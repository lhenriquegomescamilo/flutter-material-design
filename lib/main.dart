import 'package:flutter/material.dart';

import 'BodyWidgetState.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.deepOrange),
        home: Scaffold(
            drawer: Drawer(
              child: Column(
                children: <Widget>[
                  UserAccountsDrawerHeader(
                      currentAccountPicture: CircleAvatar(child: Text("C")),
                      accountName: Text("Camilo"),
                      accountEmail: Text("camilo@gmail.com")),
                  ListTile(leading: Icon(Icons.menu), title: Text("Home")),
                  ListTile(leading: Icon(Icons.ac_unit), title: Text("Home")),
                  ListTile(
                      leading: Icon(Icons.access_alarms), title: Text("Home")),
                ],
              ),
            ),
            appBar: AppBar(title: Text("Luis Henrique"), actions: <Widget>[
              IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.no_sim, color: Colors.white))
            ]),
            body: BodyWidgetState(),
            floatingActionButton:
                FloatingActionButton(onPressed: () {}, child: Icon(Icons.add)),
            bottomNavigationBar:
                BottomNavigationBar(items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.ac_unit), title: Text("Camilo")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.adb), title: Text("Luis")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.android), title: Text("Henrique"))
            ])));
  }
}
