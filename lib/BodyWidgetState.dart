import 'package:flutter/material.dart';

import 'SecondWidget.dart';

class BodyWidgetState extends StatefulWidget {
  @override
  _BodyWidgetStateState createState() => _BodyWidgetStateState();
}

class _BodyWidgetStateState extends State<BodyWidgetState> {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: RaisedButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext buildContext) => SecondWidget()));
          },
          color: Colors.deepOrangeAccent,
          child: Text(
            "Navigate to",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ));
  }
}
