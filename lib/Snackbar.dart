import 'package:flutter/material.dart';

class ScnackBarWidget extends StatefulWidget {
  @override
  _ScnackBarWidgetState createState() => _ScnackBarWidgetState();
}

class _ScnackBarWidgetState extends State<ScnackBarWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(title: Text("SnackBar")),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                    textColor: Colors.white,
                    color: Colors.amber,
                    onPressed: () {
                      _scaffoldKey.currentState.showSnackBar(
                          SnackBar(content: Text("Olá !!! mano doido")));
                    },
                    child: Text("Hey SnackBar 1")),
                Builder(
                    builder: (BuildContext internalContext) => RaisedButton(
                        textColor: Colors.white,
                        color: Colors.black38,
                        onPressed: () {
                          Scaffold.of(internalContext).showSnackBar(
                              SnackBar(content: Text("Funcionou de novo")));
                        },
                        child: Text("Hey SnackBar 2"))),
                RaisedButton(
                    textColor: Colors.white,
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                  title: Text("Ele vive"),
                                  content: Text("Esse daqui é um Dialog"),
                                  actions: <Widget>[
                                    FlatButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("OK"))
                                  ]));
                    },
                    child: Text("Hey Dialog"))
              ],
            )));
  }
}
